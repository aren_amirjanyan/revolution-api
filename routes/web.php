<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use Illuminate\Support\Facades\Artisan;

$router->get('/', function () use ($router) {
    return $router->app->version();
});

/**
 * Refresh Posts
 */
/*$router->get('/api/cron/posts/refresh', function () use ($router) {
    return Artisan::call('refresh:posts');
});*/

/**
 * Publish Posts
 */
/*$router->get('/api/cron/posts/publish', function () use ($router) {
    return Artisan::call('publish:posts');
});*/

/**
 * Share Posts
 */
/*$router->get('/api/cron/posts/share', function () use ($router) {
    return Artisan::call('share:posts');
});*/

// cors problem....
$router->options(
    '/{any:.*}',
    [
        'middleware' => ['cors'],
        function () {
            return response(['status' => 'success']);
        }
    ]
);

$router->group(['prefix' => 'api', 'middleware' => 'cors'], function () use ($router) {

    $router->group(['middleware' => 'auth'], function () use ($router) {
        /*Users*/
        $router->get('users', ['uses' => 'UserController@getAllUsers']);

        $router->get('users/{id}', ['uses' => 'UserController@getOneUser']);

        $router->post('users', ['uses' => 'UserController@create']);

        $router->delete('users/{id}', ['uses' => 'UserController@delete']);

        $router->put('users/{id}', ['uses' => 'UserController@update']);

        /*Posts*/
        $router->get('posts', ['uses' => 'PostController@getAllPosts']);

        $router->get('posts/{id}', ['uses' => 'PostController@getOnePost']);

        $router->post('posts', ['uses' => 'PostController@create']);

        $router->delete('posts', ['uses' => 'PostController@deleteAll']);

        $router->delete('posts/{id}', ['uses' => 'PostController@delete']);

        $router->put('posts', ['uses' => 'PostController@updateAll']);

        $router->put('posts/{id}', ['uses' => 'PostController@update']);

    });

    /*Auth*/
    $router->post('login', ['uses' => 'AuthController@login']);
});
