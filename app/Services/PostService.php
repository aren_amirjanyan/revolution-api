<?php

namespace App\Services;

use App\Contracts\PostServiceInterface;
use App\Core\Services\BaseService;
use App\Repositories\PostRepository;
use DOMDocument;

class PostService extends BaseService implements PostServiceInterface
{

    const SECTIONS_WITH_ID = [
        'single-post-featured-image',
        'main-content',
        'i-content'
    ];
    const SECTIONS_WITH_CLASS = [
        'featured-image',
        'entry-content',
        'single-post-image col-md-9  animation-element',
        'pic1-cont',
        'descer2',
        'i-content',
        'list__inner-box list__inner-box--with-top-line db pr',
        'entry-content clearfix'
    ];

    private $postRepository;

    /**
     * PostService constructor.
     * @param PostRepository $postRepository
     */
    public function __construct(
        PostRepository $postRepository
    )
    {
        $this->postRepository = $postRepository;
    }

    /**
     * @param $status
     * @param $page
     * @return \App\Models\Post[]|\Illuminate\Database\Eloquent\Collection
     * @throws \App\Exceptions\PostNotFoundException
     */
    public function getAllPosts($status, $page)
    {
        return $this->postRepository->getAllPosts($status, $page);
    }

    /**
     * @param $status
     * @param $page
     * @return mixed
     * @throws \App\Exceptions\PostNotFoundException
     */
    public function getAllPostsIds($status, $page)
    {
        return $this->postRepository->getAllPostsIds($status, $page);
    }

    /**
     * @param $id
     * @return mixed
     * @throws \App\Exceptions\PostNotFoundException
     */
    public function getPostById($id)
    {
        return $this->postRepository->getPostById($id);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function createPost($data)
    {
        return $this->postRepository->createPost($data);
    }

    /** Update All Posts
     * @param $data
     * @return mixed
     * @throws \App\Exceptions\PostNotFoundException
     */
    public function updateAllPosts($data)
    {
        $response = [];
        if (!empty($data['posts'])) {
            foreach ($data['posts'] as $key => $value) {
                $this->postRepository->updatePost($value['post'], $value['data']);
                $response[$key]->postId = $value;
            }
        }
        return $response;
    }

    /**
     * @param $id
     * @param $data
     * @return mixed
     * @throws \App\Exceptions\PostNotFoundException
     */
    public function updatePost($id, $data)
    {
        return $this->postRepository->updatePost($id, $data);
    }

    /** Refresh All posts
     * @param $data
     * @return mixed
     * @throws \App\Exceptions\PostNotFoundException
     */
    public function refreshAllPosts($data)
    {
        $response = [];
        if (!empty($data['posts'])) {
            foreach ($data['posts'] as $key => $value) {
                $response[$key] = $this->refreshPost($value);
                $response[$key]->postId = $value;
                $this->updatePost($value, ['post_status' => 'publish']);
            }
        }
        return $response;
    }

    public function refreshPost($id)
    {
        $post = $this->postRepository->getPostById($id);
        $dom = new DOMDocument;
        @$dom->loadHTML($post->post_content);
        $postOriginalUrl = '';

        foreach ($dom->getElementsByTagName('a') as $node) {
            $postOriginalUrl = $node->getAttribute('href');
        }
        ini_set('allow_url_fopen', 1);
        ini_set('max_execution_time', 0);
        ini_set('memory_limit', '2048M');
        $originalPostContent = $this->qr_loadUrl($postOriginalUrl);
        $originalDom = new DOMDocument;
        @$originalDom->loadHTML($originalPostContent);

        $originalPostFeaturedImage = [];

        foreach ($originalDom->getElementsByTagName('div') as $node) {
            $divId = $node->getAttribute('id');
            $divClass = $node->getAttribute('class');
            if (in_array($divId, self::SECTIONS_WITH_ID) || in_array($divClass, self::SECTIONS_WITH_CLASS)) {
                foreach ($node->getElementsByTagName('img') as $nodeImg) {
                    if ($divClass === 'list__inner-box list__inner-box--with-top-line db pr') {
                        $originalPostFeaturedImage[] = parse_url($postOriginalUrl)['host'] . $nodeImg->getAttribute('src');
                    } else {
                        $originalPostFeaturedImage[] = $nodeImg->getAttribute('src');
                    }
                }
            }
        }
        $response = [];

        if (!empty($originalPostFeaturedImage)) {
            $fileName = "featured-image-for-post-$id.jpg";

            $currentYear = gmdate("Y", time() + 3600 * (4 + date("I")));
            $currentMonth = gmdate("m", time() + 3600 * (4 + date("I")));

            $filePathForDownload = env('FILE_PATH_FOR_DOWNLAD') . DIRECTORY_SEPARATOR . $currentYear . DIRECTORY_SEPARATOR . $currentMonth . DIRECTORY_SEPARATOR . $fileName;
            $filePathForPostShow = env('FILE_PATH_FOR_POST_SHOW') . DIRECTORY_SEPARATOR . $currentYear . DIRECTORY_SEPARATOR . $currentMonth . DIRECTORY_SEPARATOR . $fileName;
            $data = $this->curl_get_file_contents($this->addHttp($originalPostFeaturedImage[0]));
            if ($data) {
                if (file_exists($filePathForDownload)) {
                    $handle = fopen($filePathForDownload, 'a');
                } else {
                    $handle = fopen($filePathForDownload, 'w');
                }

                fwrite($handle, $data);
                fclose($handle);

                $response = $this->postRepository->refreshPost($id, $filePathForPostShow);
            } else {
                $fileName = "revolutionLogo-NewReal-1.png";
                $response = $this->postRepository->refreshPost($id, env('FILE_PATH_FOR_POST_SHOW') . DIRECTORY_SEPARATOR . '2018' . DIRECTORY_SEPARATOR . '05' . DIRECTORY_SEPARATOR . $fileName);
            }
        } else {
            $fileName = "revolutionLogo-NewReal-1.png";
            $response = $this->postRepository->refreshPost($id, env('FILE_PATH_FOR_POST_SHOW') . DIRECTORY_SEPARATOR . '2018' . DIRECTORY_SEPARATOR . '05' . DIRECTORY_SEPARATOR . $fileName);
        }
        return $response;
    }

    /** Publish All Posts
     * @param $data
     * @return mixed
     * @throws \App\Exceptions\PostNotFoundException
     */
    public function publishAllPosts($data)
    {
        $response = [];
        $publishData = ['post_status' => $data['action']];

        if (!empty($data['posts'])) {
            foreach ($data['posts'] as $key => $value) {
                $response[$key] = $this->postRepository->updatePost($value, $publishData);
                $response[$key]->postId = $value;
            }
        }
        return $response;
    }

    /**
     * @param $id
     * @param $data
     * @return mixed
     * @throws \App\Exceptions\PostNotFoundException
     */
    public function publishPost($id, $data)
    {
        $publishData = ['post_status' => $data['action']];
        return $this->postRepository->updatePost($id, $publishData);
    }

    /**
     * @param $posts
     * @return mixed
     * @throws \App\Exceptions\PostNotFoundException
     */
    public function shareAllPosts($posts)
    {
        $facebookPage = 'ArmenianRevolution';
        $access_token = 'EAACv93cwjmsBADUoF9OISdtamUBt9ZCfV20ronEVORyk0CcPxnbg3pNLaPXtFffKHtDquuZBvSs4TuSq4LZC4PJZC9lgZBdwJcZC5gI0211B7C8gsF249g8OWXDlCI08zrgG0hMk26zT37ZC1lxb9RdZB9rsFPERkMysI6UvghCkxgZDZD';

        $response = [];

        if (count($posts)) {

            foreach ($posts as $key => $value) {

                if (abs(strtotime($value->post_date) - time()) / 60 <= 20) {

                    $postDate = date('Y/m/d', strtotime($value->post_date));

                    $postName = $value->post_name;

                    $ch = curl_init();

                    curl_setopt($ch, CURLOPT_URL, "https://graph.facebook.com/v3.0/$facebookPage/feed?access_token=$access_token");
                    curl_setopt($ch, CURLOPT_POST, 1);
                    curl_setopt($ch, CURLOPT_POSTFIELDS,
                        http_build_query([
                                'debug' => 'all',
                                'format' => 'json',
                                'link' => "https://revolution.am/$postDate/$postName",
                                'message' => $value->post_title,
                                'method' => 'post',
                                'pretty' => 0,
                                'published' => 1,
                                'suppress_http_code' => 1,
                                'transport' => 'cors',
                            ]
                        ));
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                    $server_output = curl_exec($ch);

                    $response[] = $server_output;

                    curl_close($ch);
                }
            }
        }

        return $response;

    }

    /** Delete All Posts
     * @param $data
     * @return mixed
     * @throws \App\Exceptions\PostNotFoundException
     */
    public function deletePosts($data)
    {
        $response = [];
        if (!empty($data)) {
            foreach ($data as $key => $value) {
                if ($this->postRepository->deletePost($value)) {
                    $response[$key]['postId'] = $value;
                }
            }
        }
        return $response;
    }

    /**
     * @param $id
     * @throws \App\Exceptions\PostNotFoundException
     */
    public function deletePost($id)
    {
        return $this->postRepository->deletePost($id);
    }

    /**
     * @param $url
     * @param string $cookiesIn
     * @return mixed
     */
    private function get_web_page($url, $cookiesIn = '')
    {
        $options = array(
            CURLOPT_RETURNTRANSFER => true,     // return web page
            CURLOPT_HEADER => true,     //return headers in addition to content
            CURLOPT_FOLLOWLOCATION => true,     // follow redirects
            CURLOPT_ENCODING => "",       // handle all encodings
            CURLOPT_AUTOREFERER => true,     // set referer on redirect
            CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
            CURLOPT_TIMEOUT => 120,      // timeout on response
            CURLOPT_MAXREDIRS => 10,       // stop after 10 redirects
            CURLINFO_HEADER_OUT => true,
            CURLOPT_SSL_VERIFYPEER => true,     // Validate SSL Cert
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_COOKIE => $cookiesIn
        );

        $ch = curl_init($url);
        curl_setopt_array($ch, $options);
        $rough_content = curl_exec($ch);
        $err = curl_errno($ch);
        $errmsg = curl_error($ch);
        $header = curl_getinfo($ch);
        curl_close($ch);

        $header_content = substr($rough_content, 0, $header['header_size']);
        $body_content = trim(str_replace($header_content, '', $rough_content));
        $pattern = "#Set-Cookie:\\s+(?<cookie>[^=]+=[^;]+)#m";
        preg_match_all($pattern, $header_content, $matches);
        $cookiesOut = implode("; ", $matches['cookie']);

        $header['errno'] = $err;
        $header['errmsg'] = $errmsg;
        $header['headers'] = $header_content;
        $header['content'] = $body_content;
        $header['cookies'] = $cookiesOut;
        return $header;
    }

    private function curl_get_file_contents($url)
    {
        $c = curl_init();
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_URL, $url);
        curl_setopt($c, CURLOPT_SSL_VERIFYPEER, FALSE);
        $contents = curl_exec($c);
        curl_close($c);

        if ($contents) return $contents;
        else return false;
    }

    private function qr_loadUrl($url)
    {
        if (is_callable('curl_init')) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_URL, $url);
            $data = curl_exec($ch);
            curl_close($ch);
        }
        if (empty($data) || !is_callable('curl_init')) {
            $opts = array('http' => array('header' => 'Connection: close'));
            $context = stream_context_create($opts);
            $headers = get_headers($url);
            $httpcode = substr($headers[0], 9, 3);
            if ($httpcode == '200')
                $data = file_get_contents($url, false, $context);
            else {
                $data = '{
                "div":"Error ' . $httpcode . ': Invalid Url<br />"}';
            }
        }
        return $data;
    }

    private function addHttp($url)
    {

        // Search the pattern
        if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {

            // If not exist then add http
            $url = "http://" . $url;
        }

        // Return the URL
        return $url;
    }

    private function getimg($url)
    {
        $headers[] = 'Accept: image / gif, image / x - bitmap, image / jpeg, image / pjpeg';
        $headers[] = 'Connection: Keep - Alive';
        $headers[] = 'Content - type: application / x - www - form - urlencoded;charset = UTF - 8';
        $user_agent = 'php';
        $process = curl_init($url);
        curl_setopt($process, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($process, CURLOPT_HEADER, 0);
        curl_setopt($process, CURLOPT_USERAGENT, $user_agent); //check here
        curl_setopt($process, CURLOPT_TIMEOUT, 30);
        curl_setopt($process, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($process, CURLOPT_FOLLOWLOCATION, 1);
        $return = curl_exec($process);
        curl_close($process);
        return $return;
    }
}