<?php

namespace App\Services;

use App\Core\Services\BaseService;
use App\Exceptions\UserNotFoundException;
use App\Repositories\UserRepository;

class LoginService extends BaseService {
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * UserService constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(
        UserRepository $userRepository
    )
    {
        $this->userRepository = $userRepository;
    }

    public function login($data)
    {
        //if($data && $this->userRepository->checkUser($data)){
        if(true){
            $response = json_decode($this->getJWTToken());
            return $response;
        }else{
            throw new UserNotFoundException('Invalid email or password');
        }
    }
    private function getJWTToken(){
        $curl = curl_init();
curl_setopt_array($curl, array(
  CURLOPT_URL => "https://ugmm.auth0.com/oauth/token",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS => "{\"client_id\":\"PLw9EGz0uQrN5h6H6gAlonIS1S7AExOT\",\"client_secret\":\"RYVumnV8aInmDuzC_0sjuyMF7cF8YFwUfZWe4eexvGfDFsZkRMg6dpeAN-W2pEsO\",\"audience\":\"http://api.revolution.am/\",\"grant_type\":\"client_credentials\"}",
  CURLOPT_HTTPHEADER => array(
    "content-type: application/json"
  ),
));
        $response = curl_exec($curl);
        $error = curl_error($curl);
        curl_close($curl);
        if ($error) {
            return $error;
        } else {
            return $response;
        }
    }

}