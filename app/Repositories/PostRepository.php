<?php

namespace App\Repositories;

use App\Core\Repositories\BaseRepository;
use App\Exceptions\PostNotFoundException;
use App\Models\Post;


class PostRepository extends BaseRepository
{

    /**
     * @param $status
     * @param $page
     * @return mixed
     * @throws PostNotFoundException
     */
    public function getAllPosts($status, $page)
    {
        $posts = Post::where(['post_type' => Post::POST_TYPE, 'post_status' => $status])
            ->orderBy('ID', 'desc')
            ->paginate(3);
        if (!count($posts))
            throw new PostNotFoundException('Post not found', 404);
        foreach ($posts as $post) {
            $post->featured_image = Post::getFeaturedImage($post->ID);
        }
        return $posts;
    }

    /**
     * @param $status
     * @param $page
     * @return mixed
     * @throws PostNotFoundException
     */
    public function getAllPostsIds($status, $page)
    {
        return Post::select('ID')->where(['post_type' => Post::POST_TYPE, 'post_status' => $status])
            ->orderBy('ID', 'desc')->limit(3)->get();
    }

    /**
     * @param $id
     * @return mixed
     * @throws PostNotFoundException
     */
    public function getPostById($id)
    {
        $post = Post::find($id);
        if (!$post)
            throw new PostNotFoundException('Post not found', 404);
        return $post;
    }

    public function createPost($data)
    {
        return Post::create($data);
    }

    /**
     * @param $id
     * @param $data
     * @return mixed
     * @throws PostNotFoundException
     */
    public function updatePost($id, $data)
    {
        $post = Post::find($id);
        if (!$post)
            throw new PostNotFoundException('Post not found', 404);
        Post::where(['ID' => $id])->update([
            'post_status' => $data['post_status'],
            'post_name' => $this->sanitize($post->post_title),
            'post_modified' => date('Y-m-d h:s:i', time()),
            'post_modified_gmt' => date('Y-m-d h:s:i', time())
        ]);
        // Debug
        // https://developers.facebook.com/tools/debug/?q=
        return $post;
    }

    /**
     * @param $id
     * @throws PostNotFoundException
     */
    public function deletePost($id)
    {
        $post = Post::find($id);
        if (!$post)
            throw new PostNotFoundException('Post not found', 404);
        return Post::where(['ID' => $id])->delete();
    }

    public function refreshPost($id, $image)
    {

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, "https://revolution.am/wp-json/revolution/api/v1/posts");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,
            http_build_query([
                    'post_id' => abs(intval($id)),
                    'image' => $image
                ]
            ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = curl_exec($ch);

        curl_close($ch);

        return json_decode($server_output);
    }

    //taken from wordpress
    private function utf8_uri_encode($utf8_string, $length = 0)
    {
        $unicode = '';
        $values = array();
        $num_octets = 1;
        $unicode_length = 0;

        $string_length = strlen($utf8_string);
        for ($i = 0; $i < $string_length; $i++) {

            $value = ord($utf8_string[$i]);

            if ($value < 128) {
                if ($length && ($unicode_length >= $length))
                    break;
                $unicode .= chr($value);
                $unicode_length++;
            } else {
                if (count($values) == 0) $num_octets = ($value < 224) ? 2 : 3;

                $values[] = $value;

                if ($length && ($unicode_length + ($num_octets * 3)) > $length)
                    break;
                if (count($values) == $num_octets) {
                    if ($num_octets == 3) {
                        $unicode .= '%' . dechex($values[0]) . '%' . dechex($values[1]) . '%' . dechex($values[2]);
                        $unicode_length += 9;
                    } else {
                        $unicode .= '%' . dechex($values[0]) . '%' . dechex($values[1]);
                        $unicode_length += 6;
                    }

                    $values = array();
                    $num_octets = 1;
                }
            }
        }

        return $unicode;
    }

//taken from wordpress
    private function seems_utf8($str)
    {
        $length = strlen($str);
        for ($i = 0; $i < $length; $i++) {
            $c = ord($str[$i]);
            if ($c < 0x80) $n = 0; # 0bbbbbbb
            elseif (($c & 0xE0) == 0xC0) $n = 1; # 110bbbbb
            elseif (($c & 0xF0) == 0xE0) $n = 2; # 1110bbbb
            elseif (($c & 0xF8) == 0xF0) $n = 3; # 11110bbb
            elseif (($c & 0xFC) == 0xF8) $n = 4; # 111110bb
            elseif (($c & 0xFE) == 0xFC) $n = 5; # 1111110b
            else return false; # Does not match any model
            for ($j = 0; $j < $n; $j++) { # n bytes matching 10bbbbbb follow ?
                if ((++$i == $length) || ((ord($str[$i]) & 0xC0) != 0x80))
                    return false;
            }
        }
        return true;
    }

    //function sanitize_title_with_dashes taken from wordpress
    private function sanitize($title)
    {
        $title = strip_tags($title);
        // Preserve escaped octets.
        $title = preg_replace('|%([a-fA-F0-9][a-fA-F0-9])|', '---$1---', $title);
        // Remove percent signs that are not part of an octet.
        $title = str_replace('%', '', $title);
        // Restore octets.
        $title = preg_replace('|---([a-fA-F0-9][a-fA-F0-9])---|', '%$1', $title);

        if ($this->seems_utf8($title)) {
            if (function_exists('mb_strtolower')) {
                $title = mb_strtolower($title, 'UTF-8');
            }
            $title = $this->utf8_uri_encode($title, 200);
        }

        $title = strtolower($title);
        $title = preg_replace('/&.+?;/', '', $title); // kill entities
        $title = str_replace('.', '-', $title);
        $title = preg_replace('/[^%a-z0-9 _-]/', '', $title);
        $title = preg_replace('/\s+/', '-', $title);
        $title = preg_replace('|-+|', '-', $title);
        $title = trim($title, '-');

        return $title;
    }
}