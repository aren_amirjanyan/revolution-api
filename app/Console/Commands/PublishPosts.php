<?php

namespace App\Console\Commands;

use App\Services\PostService;
use Illuminate\Console\Command;

class PublishPosts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'publish:posts';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Publish pending posts in https://revolution.am site';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param PostService $postService
     * @throws \App\Exceptions\PostNotFoundException
     */
    public function handle(PostService $postService)
    {
        $posts = $postService->getAllPostsIds('pending', 1);

        if ($posts) {
            $postsIds = collect($posts)->map(function ($post) {
                return $post->ID;
            });
            if ($postService->publishAllPosts(['posts' => $postsIds, 'action' => 'publish'])) {
                echo "The pending posts published!";
            }
        }
    }
}