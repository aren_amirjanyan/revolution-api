<?php

namespace App\Console\Commands;

use App\Services\PostService;
use Illuminate\Console\Command;

class RefreshPosts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'refresh:posts';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Refresh thumbnails of pending posts';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param PostService $postService
     * @throws \App\Exceptions\PostNotFoundException
     */
    public function handle(PostService $postService)
    {
        $posts = $postService->getAllPostsIds('pending', 1);

        if ($posts) {
            $postsIds = collect($posts)->map(function ($post) {
                return $post->ID;
            });
            if($postService->refreshAllPosts(['posts' => $postsIds])){
                echo "The pending posts thumbnails refreshed!";
            }
        }
    }
}