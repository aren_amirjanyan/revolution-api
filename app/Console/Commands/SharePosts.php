<?php
namespace App\Console\Commands;
use App\Services\PostService;
use Illuminate\Console\Command;

class SharePosts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'share:posts';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Share published posts in ArmenianRevolution Facebook page';
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param PostService $postService
     * @throws \App\Exceptions\PostNotFoundException
     */
    public function handle(PostService $postService)
    {
        $posts = $postService->getAllPosts('publish', 1);

        if ($posts) {
            $sharedPosts = $postService->shareAllPosts($posts);
            if (count($sharedPosts)) {
                echo "The latest published posts shared in facebook!";
            }
        }
    }
}