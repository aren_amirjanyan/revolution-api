<?php

namespace App\Console;

use App\Console\Commands\PublishPosts;
use App\Console\Commands\RefreshPosts;
use App\Console\Commands\SharePosts;
use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        RefreshPosts::class,
        PublishPosts::class,
        SharePosts::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('refresh:posts')
            ->everyFiveMinutes()
            ->appendOutputTo('/logs/refresh_posts.log');
        $schedule->command('publish:posts')
            ->everyTenMinutes()
            ->appendOutputTo('/logs/publish_posts.log');
        $schedule->command('share:posts')
            ->everyFifteenMinutes()
            ->appendOutputTo('/logs/share_posts.log');
    }
}
