<?php

namespace App\Http\Controllers;

use App\Services\LoginService;
use Illuminate\Http\Request;

class AuthController extends Controller
{

    public function login(
        Request $request,
        LoginService $loginService
    )
    {
        try {
            $this->validate($request, []);

            $data = $request->all();

            $user = $loginService->login($data);
            $response = ['data' => $user,'success' => true, 'error' => false, 'message' => ''];
            $status = 200;
        }catch (\Exception $exception){
            $response = ['data' => [],'success' => false, 'error' => true, 'message' => $exception->getMessage()];
            $status = 403;
        }
        return response()->json($response,$status);
    }
}