<?php

namespace App\Http\Controllers;

use App\Services\PostService;
use Illuminate\Http\Request;

class PostController extends Controller
{

    public function getAllPosts(
        Request $request,
        PostService $postService
    )
    {
        try {

            $status = $request->input('status');
            $page = $request->input('page');

            $posts = $postService->getAllPosts($status,$page);

            $response = ['data' => $posts,'success' => true, 'error' => false, 'message' => ''];
            $status = 200;
        }catch (\Exception $exception){
            $response = ['data' => [],'success' => false, 'error' => true, 'message' => $exception->getMessage()];
            $status = !empty($exception->status) ? $exception->status : $exception->getCode();
        }
        return response()->json($response);
    }

    public function getOnePost(
        $id,
        PostService $postService
    )
    {
        try {
            $post = $postService->getPostById($id);
            $response = ['data' => $post,'success' => true, 'error' => false, 'message' => ''];
            $status = 200;
        }catch (\Exception $exception){
            $response = ['data' => [],'success' => false, 'error' => true, 'message' => $exception->getMessage()];
            $status = !empty($exception->status) ? $exception->status : $exception->getCode();
        }
        return response()->json($response,$status);
    }

    public function create(
        Request $request,
        PostService $postService
    )
    {
        try {
            $this->validate($request, []);

            $data = $request->all();

            $post = $postService->createPost($data);
            $response = ['data' => $post,'success' => true, 'error' => false, 'message' => 'post successfully created!'];
            $status = 201;
        }catch (\Exception $exception){
            $response = ['data' => [],'success' => false, 'error' => true, 'message' => $exception->getMessage()];
            $status = !empty($exception->status) ? $exception->status : 200;
        }
        return response()->json($response,$status);
    }

    public function updateAll(
        Request $request,
        PostService $postService
    )
    {
        try {
            $this->validate($request, []);

            $data = $request->all();

            switch ($data['action']) {
                case 'refresh':
                    $posts = $postService->refreshAllPosts($data);
                    break;
                case 'publish':
                    $posts = $postService->publishAllPosts($data);
                    break;
                default:
                    $posts = $postService->updateAllPosts($data);
                    break;
            }

            $response = ['data' => $posts,'success' => true, 'error' => false, 'message' => 'posts successfully updated!'];
            $status = 200;
        }catch (\Exception $exception){
            $response = ['data' => [],'success' => false, 'error' => true, 'message' => $exception->getMessage()];
            $status = !empty($exception->status) ? $exception->status : 200;
        }
        return response()->json($response,$status);
    }

    public function update(
        $id,
        Request $request,
        PostService $postService
    )
    {
        try {
            $this->validate($request, []);

            $data = $request->all();

            switch ($data['action']) {
                case 'refresh':
                    $post = $postService->refreshPost($id,$data);
                    break;
                case 'publish':
                    $post = $postService->publishPost($id,$data);
                    break;
                default:
                    $post = $postService->updatePost($id,$data);
                    break;
            }

            $response = ['data' => $post,'success' => true, 'error' => false, 'message' => 'post successfully updated!'];
            $status = 200;
        }catch (\Exception $exception){
            $response = ['data' => [],'success' => false, 'error' => true, 'message' => $exception->getMessage()];
            $status = !empty($exception->status) ? $exception->status : 200;
        }
        return response()->json($response,$status);
    }

    public function deleteAll(
        Request $request,
        PostService $postService
    )
    {
        try {

            $this->validate($request, []);

            $data = $request->all();

            $posts = $postService->deletePosts($data);

            $response = ['data' => $posts,'success' => true, 'error' => false, 'message' => 'posts successfully deleted!'];
            $status = 200;
        }catch (\Exception $exception){
            $response = ['data' => [],'success' => false, 'error' => true, 'message' => $exception->getMessage()];
            $status = !empty($exception->status) ? $exception->status : 200;
        }
        return response()->json($response,$status);
    }
    public function delete(
        $id,
        PostService $postService
    )
    {
        try {
            $postService->deletePost($id);
            $response = ['data' => [],'success' => true, 'error' => false, 'message' => 'post successfully deleted!'];
            $status = 200;
        }catch (\Exception $exception){
            $response = ['data' => [],'success' => false, 'error' => true, 'message' => $exception->getMessage()];
            $status = !empty($exception->status) ? $exception->status : 200;
        }
        return response()->json($response,$status);
    }
}