<?php

namespace App\Providers;

use App\Contracts\PostServiceInterface;
use App\Services\PostService;
use Illuminate\Support\ServiceProvider;

class PostServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton(PostServiceInterface::class,PostService::class);
    }
}
