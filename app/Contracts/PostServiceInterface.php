<?php


namespace App\Contracts;

interface PostServiceInterface
{
    public function getAllPosts($status,$page);
    public function getAllPostsIds($status,$page);
    public function getPostById($id);
    public function createPost($data);
    public function updatePost($id,$data);
    public function updateAllPosts($data);
    public function deletePost($id);

}