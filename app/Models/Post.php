<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    const POST_TYPE = 'post';
    const POST_STATUS = 'pending';

    protected $table = 'posts';

    protected $fillable = [
        'post_parent',
        'post_author',
        'post_name',
        'post_title',
        'comment_status',
        'ping_status',
        'post_date',
        'post_date_gmt',
        'post_modified',
        'post_modified_gmt',
        'post_mime_type',
        'post_status',
        'post_type',
        'guid'
    ];

    public $timestamps = false;

    public static function getFeaturedImage($postId){
        $featuredImage = PostMeta::select('posts.guid')
                            ->join('posts','posts.id','=','postmeta.meta_value')
                            ->where(['postmeta.post_id' => $postId, 'meta_key' => '_thumbnail_id'])
                            ->first();
        return $featuredImage;
    }
}
