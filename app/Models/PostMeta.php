<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostMeta extends Model
{
    protected $table = 'postmeta';

    protected $fillable = [
        'meta_id','post_id','meta_key','meta_value'
    ];

    public $timestamps = false;
}
